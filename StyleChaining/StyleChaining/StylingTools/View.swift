//
//  View.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct View {
        public static let backgroundColor: (UIColor?) -> (UIView) -> Void
            = set(\.backgroundColor)
        
        public static let contentMode: (UIView.ContentMode) -> (UIView) -> Void
            = set(\.contentMode)
        
        public static let alpha: (CGFloat) -> (UIView) -> Void
            = set(\.alpha)
        
        public static let cornerRadius: (CGFloat) -> (UIView) -> Void
            = set(\.layer.cornerRadius)
        
        public static let clipsToBounds: (Bool) -> (UIView) -> Void
            = set(\.clipsToBounds)
        
        public static let translatesAutoresizingMaskIntoConstraints: (Bool) -> (UIView) -> Void
            = set(\.translatesAutoresizingMaskIntoConstraints)
        
        public static let isHidden: (Bool) -> (UIView) -> Void
            = set(\.isHidden)
        
        public static let isUserInteractionEnabled: (Bool) -> (UIView) -> Void
            = set(\.isUserInteractionEnabled)
        
        public static let borderColor: (CGColor?) -> (UIView) -> Void
            = set(\.layer.borderColor)
        
        public static let borderWidth: (CGFloat) -> (UIView) -> Void
            = set(\.layer.borderWidth)
        
        public static let masksToBounds: (Bool) -> (UIView) -> Void
            = set(\.layer.masksToBounds)
        
        // MARK: - Composited
        
        public static let roundedRect: (CGFloat) -> (UIView) -> Void = {
            StylingTools.View.cornerRadius($0)
                <> StylingTools.View.clipsToBounds(true)
        }
        
        public static let border: (CGColor?, CGFloat) -> (UIView) -> Void = {
            StylingTools.View.borderColor($0)
                <> StylingTools.View.borderWidth($1)
                <> StylingTools.View.masksToBounds($0 != nil)
        }
    }
}
