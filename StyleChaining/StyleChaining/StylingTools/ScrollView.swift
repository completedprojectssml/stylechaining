//
//  ScrollView.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct ScrollView {
        public static let alwaysBounceHorizontal: (Bool) -> (UIScrollView) -> Void
            = set(\.alwaysBounceHorizontal)
        
        public static let alwaysBounceVertical: (Bool) -> (UIScrollView) -> Void
            = set(\.alwaysBounceVertical)
        
        public static let bounces: (Bool) -> (UIScrollView) -> Void
            = set(\.bounces)
        
        public static let bouncesZoom: (Bool) -> (UIScrollView) -> Void
            = set(\.bouncesZoom)
        
        public static let canCancelContentTouches: (Bool) -> (UIScrollView) -> Void
            = set(\.canCancelContentTouches)
        
        public static let isDirectionalLockEnabled: (Bool) -> (UIScrollView) -> Void
            = set(\.isDirectionalLockEnabled)
        
        public static let isPagingEnabled: (Bool) -> (UIScrollView) -> Void
            = set(\.isPagingEnabled)
        
        public static let isScrollEnabled: (Bool) -> (UIScrollView) -> Void
            = set(\.isScrollEnabled)
        
        public static let showsHorizontalScrollIndicator: (Bool) -> (UIScrollView) -> Void
            = set(\.showsHorizontalScrollIndicator)
        
        public static let showsVerticalScrollIndicator: (Bool) -> (UIScrollView) -> Void
            = set(\.showsVerticalScrollIndicator)
        
        public static let indicatorStyle: (UIScrollView.IndicatorStyle) -> (UIScrollView) -> Void
            = set(\.indicatorStyle)
        
        public static let keyboardDismissMode: (UIScrollView.KeyboardDismissMode) -> (UIScrollView) -> Void
            = set(\.keyboardDismissMode)
        
        public static let refreshControl: (UIRefreshControl?) -> (UIScrollView) -> Void
            = set(\.refreshControl)
        
        public static let maximumZoomScale: (CGFloat) -> (UIScrollView) -> Void
            = set(\.maximumZoomScale)
        
        public static let minimumZoomScale: (CGFloat) -> (UIScrollView) -> Void
            = set(\.minimumZoomScale)
    }
}
