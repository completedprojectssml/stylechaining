//
//  Image.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct ImageView {
        public static let adjustsImageSizeForAccessibilityContentSizeCategory: (Bool) -> (UIImageView) -> Void
            = set(\.adjustsImageSizeForAccessibilityContentSizeCategory)
        
        public static let isHighlighted: (Bool) -> (UIImageView) -> Void
            = set(\.isHighlighted)
        
        public static let isUserInteractionEnabled: (Bool) -> (UIImageView) -> Void
            = set(\.isUserInteractionEnabled)
        
        public static let animationDuration: (TimeInterval) -> (UIImageView) -> Void
            = set(\.animationDuration)
        
        public static let animationRepeatCount: (Int) -> (UIImageView) -> Void
            = set(\.animationRepeatCount)
        
        public static let tintColor: (UIColor) -> (UIImageView) -> Void
            = set(\.tintColor)
        
        public static let animationImages: ([UIImage]?) -> (UIImageView) -> Void
            = set(\.animationImages)
        
        public static let highlightedAnimationImages: ([UIImage]?) -> (UIImageView) -> Void
            = set(\.highlightedAnimationImages)
        
        public static let highlightedImage: (UIImage?) -> (UIImageView) -> Void
            = set(\.highlightedImage)
        
        public static let image: (UIImage?) -> (UIImageView) -> Void
            = set(\.image)
    }
}
