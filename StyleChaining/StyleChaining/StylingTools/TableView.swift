//
//  TableView.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct TableView {
        public static let allowsMultipleSelection: (Bool) -> (UITableView) -> Void
            = set(\.allowsMultipleSelection)
        
        public static let allowsMultipleSelectionDuringEditing: (Bool) -> (UITableView) -> Void
            = set(\.allowsMultipleSelectionDuringEditing)
        
        public static let allowsSelection: (Bool) -> (UITableView) -> Void
            = set(\.allowsSelection)
        
        public static let allowsSelectionDuringEditing: (Bool) -> (UITableView) -> Void
            = set(\.allowsSelectionDuringEditing)
        
        public static let dragInteractionEnabled: (Bool) -> (UITableView) -> Void
            = set(\.dragInteractionEnabled)
        
        public static let isEditing: (Bool) -> (UITableView) -> Void
            = set(\.isEditing)
        
        public static let sectionIndexBackgroundColor: (UIColor?) -> (UITableView) -> Void
            = set(\.sectionIndexBackgroundColor)
        
        public static let sectionIndexColor: (UIColor?) -> (UITableView) -> Void
            = set(\.sectionIndexColor)
        
        public static let sectionIndexTrackingBackgroundColor: (UIColor?) -> (UITableView) -> Void
            = set(\.sectionIndexTrackingBackgroundColor)
        
        public static let separatorColor: (UIColor?) -> (UITableView) -> Void
            = set(\.separatorColor)
        
        public static let separatorEffect: (UIVisualEffect?) -> (UITableView) -> Void
            = set(\.separatorEffect)
        
        public static let separatorInset: (UIEdgeInsets) -> (UITableView) -> Void
            = set(\.separatorInset)
        
        public static let separatorStyle: (UITableViewCell.SeparatorStyle) -> (UITableView) -> Void
            = set(\.separatorStyle)
        
        public static let estimatedRowHeight: (CGFloat) -> (UITableView) -> Void
            = set(\.estimatedRowHeight)
        
        public static let estimatedSectionHeaderHeight: (CGFloat) -> (UITableView) -> Void
            = set(\.estimatedSectionHeaderHeight)
        
        public static let estimatedSectionFooterHeight: (CGFloat) -> (UITableView) -> Void
            = set(\.estimatedSectionFooterHeight)
        
        public static let dataSource: (UITableViewDataSource?) -> (UITableView) -> Void
            = set(\.dataSource)
        
        public static let delegate: (UITableViewDelegate?) -> (UITableView) -> Void
            = set(\.delegate)
        
        public static let dragDelegate: (UITableViewDragDelegate?) -> (UITableView) -> Void
            = set(\.dragDelegate)
        
        public static let dropDelegate: (UITableViewDropDelegate?) -> (UITableView) -> Void
            = set(\.dropDelegate)
        
        public static let backgroundView: (UIView?) -> (UITableView) -> Void
            = set(\.backgroundView)
        
        public static let tableFooterView: (UIView?) -> (UITableView) -> Void
            = set(\.tableFooterView)
        
        public static let tableHeaderView: (UIView?) -> (UITableView) -> Void
            = set(\.tableHeaderView)
    }
    
    struct TableViewCell {
        public static let accessoryView: (UIView?) -> (UITableViewCell) -> Void
            = set(\.accessoryView)
        
        public static let editingAccessoryView: (UIView?) -> (UITableViewCell) -> Void
            = set(\.editingAccessoryView)
        
        public static let backgroundView: (UIView?) -> (UITableViewCell) -> Void
            = set(\.backgroundView)
        
        public static let multipleSelectionBackgroundView: (UIView?) -> (UITableViewCell) -> Void
            = set(\.multipleSelectionBackgroundView)
        
        public static let selectedBackgroundView: (UIView?) -> (UITableViewCell) -> Void
            = set(\.selectedBackgroundView)
        
        public static let focusStyle: (UITableViewCell.FocusStyle) -> (UITableViewCell) -> Void
            = set(\.focusStyle)
        
        public static let indentationLevel: (Int) -> (UITableViewCell) -> Void
            = set(\.indentationLevel)
        
        public static let indentationWidth: (CGFloat) -> (UITableViewCell) -> Void
            = set(\.indentationWidth)
        
        public static let isEditing: (Bool) -> (UITableViewCell) -> Void
            = set(\.isEditing)
        
        public static let isSelected: (Bool) -> (UITableViewCell) -> Void
            = set(\.isSelected)
        
        public static let isHighlighted: (Bool) -> (UITableViewCell) -> Void
            = set(\.isHighlighted)
        
        public static let shouldIndentWhileEditing: (Bool) -> (UITableViewCell) -> Void
            = set(\.shouldIndentWhileEditing)
        
        public static let showsReorderControl: (Bool) -> (UITableViewCell) -> Void
            = set(\.showsReorderControl)
        
        public static let userInteractionEnabledWhileDragging: (Bool) -> (UITableViewCell) -> Void
            = set(\.userInteractionEnabledWhileDragging)
        
        public static let accessoryType: (UITableViewCell.AccessoryType) -> (UITableViewCell) -> Void
            = set(\.accessoryType)
        
        public static let editingAccessoryType: (UITableViewCell.AccessoryType) -> (UITableViewCell) -> Void
            = set(\.editingAccessoryType)
        
        public static let selectionStyle: (UITableViewCell.SelectionStyle) -> (UITableViewCell) -> Void
            = set(\.selectionStyle)
        
        public static let separatorInset: (UIEdgeInsets) -> (UITableViewCell) -> Void
            = set(\.separatorInset)
    }
}
