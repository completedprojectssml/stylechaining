//
//  StackView.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 22/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct StackView {
        public static let alignment: (UIStackView.Alignment) -> (UIStackView) -> Void
            = set(\.alignment)
        
        public static let axis: (NSLayoutConstraint.Axis) -> (UIStackView) -> Void
            = set(\.axis)
        
        public static let distribution: (UIStackView.Distribution) -> (UIStackView) -> Void
            = set(\.distribution)
        
        public static let isBaselineRelativeArrangement: (Bool) -> (UIStackView) -> Void
            = set(\.isBaselineRelativeArrangement)
        
        public static let isLayoutMarginsRelativeArrangement: (Bool) -> (UIStackView) -> Void
            = set(\.isLayoutMarginsRelativeArrangement)
        
        public static let spacing: (CGFloat) -> (UIStackView) -> Void
            = set(\.spacing)
    }
}
