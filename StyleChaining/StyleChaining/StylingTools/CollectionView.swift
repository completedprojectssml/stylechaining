//
//  CollectionView.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct CollectionView {
        public static let allowsMultipleSelection: (Bool) -> (UICollectionView) -> Void
            = set(\.allowsMultipleSelection)
        
        public static let allowsSelection: (Bool) -> (UICollectionView) -> Void
            = set(\.allowsSelection)
        
        public static let dragInteractionEnabled: (Bool) -> (UICollectionView) -> Void
            = set(\.dragInteractionEnabled)
        
        public static let dataSource: (UICollectionViewDataSource?) -> (UICollectionView) -> Void
            = set(\.dataSource)
        
        public static let delegate: (UICollectionViewDelegate?) -> (UICollectionView) -> Void
            = set(\.delegate)
        
        public static let dragDelegate: (UICollectionViewDragDelegate?) -> (UICollectionView) -> Void
            = set(\.dragDelegate)
        
        public static let dropDelegate: (UICollectionViewDropDelegate?) -> (UICollectionView) -> Void
            = set(\.dropDelegate)
        
        public static let backgroundView: (UIView?) -> (UICollectionView) -> Void
            = set(\.backgroundView)
        
        public static let collectionViewLayout: (UICollectionViewLayout) -> (UICollectionView) -> Void
            = set(\.collectionViewLayout)
        
        public static let isPrefetchingEnabled: (Bool) -> (UICollectionView) -> Void
            = set(\.isPrefetchingEnabled)
    }
    
    struct CollectionViewFlowLayout {
        public static let estimatedItemSize: (CGSize) -> (UICollectionViewFlowLayout) -> Void
            = set(\.estimatedItemSize)
        
        public static let footerReferenceSize: (CGSize) -> (UICollectionViewFlowLayout) -> Void
            = set(\.footerReferenceSize)
        
        public static let headerReferenceSize: (CGSize) -> (UICollectionViewFlowLayout) -> Void
            = set(\.headerReferenceSize)
        
        public static let minimumInteritemSpacing: (CGFloat) -> (UICollectionViewFlowLayout) -> Void
            = set(\.minimumInteritemSpacing)
        
        public static let minimumLineSpacing: (CGFloat) -> (UICollectionViewFlowLayout) -> Void
            = set(\.minimumLineSpacing)
        
        public static let scrollDirection: (UICollectionView.ScrollDirection) -> (UICollectionViewFlowLayout) -> Void
            = set(\.scrollDirection)
        
        public static let sectionFootersPinToVisibleBounds: (Bool) -> (UICollectionViewFlowLayout) -> Void
            = set(\.sectionFootersPinToVisibleBounds)
        
        public static let sectionHeadersPinToVisibleBounds: (Bool) -> (UICollectionViewFlowLayout) -> Void
            = set(\.sectionHeadersPinToVisibleBounds)
        
        public static let sectionInset: (UIEdgeInsets) -> (UICollectionViewFlowLayout) -> Void
            = set(\.sectionInset)
    }
    
    struct CollectionViewCell {
        public static let backgroundView: (UIView?) -> (UICollectionViewCell) -> Void
            = set(\.backgroundView)
        
        public static let selectedBackgroundView: (UIView?) -> (UICollectionViewCell) -> Void
            = set(\.selectedBackgroundView)
        
        public static let isHighlighted: (Bool) -> (UICollectionViewCell) -> Void
            = set(\.isHighlighted)
        
        public static let isSelected: (Bool) -> (UICollectionViewCell) -> Void
            = set(\.isSelected)
    }
}
