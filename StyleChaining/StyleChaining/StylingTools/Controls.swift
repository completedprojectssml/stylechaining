//
//  Controls.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct RefreshControl {
        public static let attributedTitle: (NSAttributedString?) -> (UIRefreshControl) -> Void
            = set(\.attributedTitle)
        
        public static let tintColor: (UIColor?) -> (UIRefreshControl) -> Void
            = set(\.tintColor)
    }
    
    struct ActivityIndicator {
        public static let style: (UIActivityIndicatorView.Style) -> (UIActivityIndicatorView) -> Void
            = set(\.style)
        
        public static let color: (UIColor) -> (UIActivityIndicatorView) -> Void
            = set(\.color)
        
        public static let hidesWhenStopped: (Bool) -> (UIActivityIndicatorView) -> Void
            = set(\.hidesWhenStopped)
    }
    
    struct Button {
        public static let adjustsImageSizeForAccessibilityContentSizeCategory: (Bool) -> (UIButton) -> Void
            = set(\.adjustsImageSizeForAccessibilityContentSizeCategory)
        
        public static let adjustsImageWhenDisabled: (Bool) -> (UIButton) -> Void
            = set(\.adjustsImageWhenDisabled)
        
        public static let adjustsImageWhenHighlighted: (Bool) -> (UIButton) -> Void
            = set(\.adjustsImageWhenHighlighted)
        
        public static let reversesTitleShadowWhenHighlighted: (Bool) -> (UIButton) -> Void
            = set(\.reversesTitleShadowWhenHighlighted)
        
        public static let showsTouchWhenHighlighted: (Bool) -> (UIButton) -> Void
            = set(\.showsTouchWhenHighlighted)
        
        public static let contentEdgeInsets: (UIEdgeInsets) -> (UIButton) -> Void
            = set(\.contentEdgeInsets)
        
        public static let imageEdgeInsets: (UIEdgeInsets) -> (UIButton) -> Void
            = set(\.imageEdgeInsets)
        
        public static let titleEdgeInsets: (UIEdgeInsets) -> (UIButton) -> Void
            = set(\.titleEdgeInsets)
        
        public static let tintColor: (UIColor) -> (UIButton) -> Void
            = set(\.tintColor)
        
        public static let attributedTitle: (NSAttributedString?) -> (UIButton) -> Void
            = { title in return { $0.setAttributedTitle(title, for: .normal) } }
        
        public static let backgroundImage: (UIImage?) -> (UIButton) -> Void
            = { image in return { $0.setBackgroundImage(image, for: .normal) } }
        
        public static let image: (UIImage?) -> (UIButton) -> Void
            = { image in return { $0.setImage(image, for: .normal) } }
        
        public static let title: (String?) -> (UIButton) -> Void
            = { title in return { $0.setTitle(title, for: .normal) } }
        
        public static let titleColor: (UIColor?) -> (UIButton) -> Void
            = { color in return { $0.setTitleColor(color, for: .normal) } }
        
        public static let titleShadowColor: (UIColor?) -> (UIButton) -> Void
            = { color in return { $0.setTitleShadowColor(color, for: .normal) } }
    }
    
    struct DatePicker {
        public static let calendar: (Calendar) -> (UIDatePicker) -> Void
            = set(\.calendar)
        
        public static let countDownDuration: (TimeInterval) -> (UIDatePicker) -> Void
            = set(\.countDownDuration)
        
        public static let date: (Date) -> (UIDatePicker) -> Void
            = set(\.date)
        
        public static let maximumDate: (Date?) -> (UIDatePicker) -> Void
            = set(\.maximumDate)
        
        public static let minimumDate: (Date?) -> (UIDatePicker) -> Void
            = set(\.minimumDate)
        
        public static let datePickerMode: (UIDatePicker.Mode) -> (UIDatePicker) -> Void
            = set(\.datePickerMode)
        
        public static let locale: (Locale?) -> (UIDatePicker) -> Void
            = set(\.locale)
        
        public static let minuteInterval: (Int) -> (UIDatePicker) -> Void
            = set(\.minuteInterval)
        
        public static let timeZone: (TimeZone?) -> (UIDatePicker) -> Void
            = set(\.timeZone)
    }
    
    struct PageControl {
        public static let currentPage: (Int) -> (UIPageControl) -> Void
            = set(\.currentPage)
        
        public static let numberOfPages: (Int) -> (UIPageControl) -> Void
            = set(\.numberOfPages)
        
        public static let currentPageIndicatorTintColor: (UIColor?) -> (UIPageControl) -> Void
            = set(\.currentPageIndicatorTintColor)
        
        public static let pageIndicatorTintColor: (UIColor?) -> (UIPageControl) -> Void
            = set(\.pageIndicatorTintColor)
        
        public static let defersCurrentPageDisplay: (Bool) -> (UIPageControl) -> Void
            = set(\.defersCurrentPageDisplay)
        
        public static let hidesForSinglePage: (Bool) -> (UIPageControl) -> Void
            = set(\.hidesForSinglePage)
    }
    
    struct SegmentControl {
        public static let apportionsSegmentWidthsByContent: (Bool) -> (UISegmentedControl) -> Void
            = set(\.apportionsSegmentWidthsByContent)
        
        public static let isMomentary: (Bool) -> (UISegmentedControl) -> Void
            = set(\.isMomentary)
        
        public static let selectedSegmentIndex: (Int) -> (UISegmentedControl) -> Void
            = set(\.selectedSegmentIndex)
        
        public static let tintColor: (UIColor) -> (UISegmentedControl) -> Void
            = set(\.tintColor)
        
        public static let backgroundImage: (UIImage?) -> (UISegmentedControl) -> Void
            = { image in return { $0.setBackgroundImage(image, for: .normal, barMetrics: .default) } }
        
        public static let titleTextAttributes: ([NSAttributedString.Key : Any]?) -> (UISegmentedControl) -> Void
            = { attributes in return { $0.setTitleTextAttributes(attributes, for: .normal) } }
    }
    
    struct Slider {
        public static let isContinuous: (Bool) -> (UISlider) -> Void
            = set(\.isContinuous)
        
        public static let maximumValue: (Float) -> (UISlider) -> Void
            = set(\.maximumValue)
        
        public static let minimumValue: (Float) -> (UISlider) -> Void
            = set(\.minimumValue)
        
        public static let value: (Float) -> (UISlider) -> Void
            = set(\.value)
        
        public static let maximumTrackImage: (UIImage?) -> (UISlider) -> Void
            = { image in return { $0.setMaximumTrackImage(image, for: .normal) } }
        
        public static let minimumTrackImage: (UIImage?) -> (UISlider) -> Void
            = { image in return { $0.setMinimumTrackImage(image, for: .normal) } }
        
        public static let setThumbImage: (UIImage?) -> (UISlider) -> Void
            = { image in return { $0.setThumbImage(image, for: .normal) } }
    }
    
    struct Stepper {
        public static let autorepeat: (Bool) -> (UIStepper) -> Void
            = set(\.autorepeat)
        
        public static let isContinuous: (Bool) -> (UIStepper) -> Void
            = set(\.isContinuous)
        
        public static let wraps: (Bool) -> (UIStepper) -> Void
            = set(\.wraps)
        
        public static let minimumValue: (Double) -> (UIStepper) -> Void
            = set(\.minimumValue)
        
        public static let maximumValue: (Double) -> (UIStepper) -> Void
            = set(\.maximumValue)
        
        public static let value: (Double) -> (UIStepper) -> Void
            = set(\.value)
        
        public static let stepValue: (Double) -> (UIStepper) -> Void
            = set(\.stepValue)
        
        public static let tintColor: (UIColor) -> (UIStepper) -> Void
            = set(\.tintColor)
        
        public static let setBackgroundImage: (UIImage?) -> (UIStepper) -> Void
            = { image in return { $0.setBackgroundImage(image, for: .normal) } }
        
        public static let setDecrementImage: (UIImage?) -> (UIStepper) -> Void
            = { image in return { $0.setDecrementImage(image, for: .normal) } }
        
        public static let setIncrementImage: (UIImage?) -> (UIStepper) -> Void
            = { image in return { $0.setIncrementImage(image, for: .normal) } }
    }
    
    struct Switch {
        public static let isOn: (Bool) -> (UISwitch) -> Void
            = set(\.isOn)
        
        public static let offImage: (UIImage?) -> (UISwitch) -> Void
            = set(\.offImage)
        
        public static let onImage: (UIImage?) -> (UISwitch) -> Void
            = set(\.onImage)
        
        public static let onTintColor: (UIColor?) -> (UISwitch) -> Void
            = set(\.onTintColor)
        
        public static let thumbTintColor: (UIColor?) -> (UISwitch) -> Void
            = set(\.thumbTintColor)
        
        public static let tintColor: (UIColor) -> (UISwitch) -> Void
            = set(\.tintColor)
    }
}
