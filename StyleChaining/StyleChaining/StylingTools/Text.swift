//
//  Text.swift
//  StyleChaining
//
//  Created by Andrew Krotov on 19/04/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import UIKit

public extension StylingTools {
    struct TextField {
        public static let adjustsFontForContentSizeCategory: (Bool) -> (UITextField) -> Void
            = set(\.adjustsFontForContentSizeCategory)
        
        public static let adjustsFontSizeToFitWidth: (Bool) -> (UITextField) -> Void
            = set(\.adjustsFontSizeToFitWidth)
        
        public static let allowsEditingTextAttributes: (Bool) -> (UITextField) -> Void
            = set(\.allowsEditingTextAttributes)
        
        public static let clearsOnBeginEditing: (Bool) -> (UITextField) -> Void
            = set(\.clearsOnBeginEditing)
        
        public static let clearsOnInsertion: (Bool) -> (UITextField) -> Void
            = set(\.clearsOnInsertion)
        
        public static let enablesReturnKeyAutomatically: (Bool) -> (UITextField) -> Void
            = set(\.enablesReturnKeyAutomatically)
        
        public static let isSecureTextEntry: (Bool) -> (UITextField) -> Void
            = set(\.isSecureTextEntry)
        
        public static let attributedPlaceholder: (NSAttributedString?) -> (UITextField) -> Void
            = set(\.attributedPlaceholder)
        
        public static let attributedText: (NSAttributedString?) -> (UITextField) -> Void
            = set(\.attributedText)
        
        public static let autocapitalizationType: (UITextAutocapitalizationType) -> (UITextField) -> Void
            = set(\.autocapitalizationType)
        
        public static let autocorrectionType: (UITextAutocorrectionType) -> (UITextField) -> Void
            = set(\.autocorrectionType)
        
        public static let background: (UIImage?) -> (UITextField) -> Void
            = set(\.background)
        
        public static let disabledBackground: (UIImage?) -> (UITextField) -> Void
            = set(\.disabledBackground)
        
        public static let borderStyle: (UITextField.BorderStyle) -> (UITextField) -> Void
            = set(\.borderStyle)
        
        public static let clearButtonMode: (UITextField.ViewMode) -> (UITextField) -> Void
            = set(\.clearButtonMode)
        
        public static let leftViewMode: (UITextField.ViewMode) -> (UITextField) -> Void
            = set(\.leftViewMode)
        
        public static let rightViewMode: (UITextField.ViewMode) -> (UITextField) -> Void
            = set(\.rightViewMode)
        
        public static let defaultTextAttributes: ([NSAttributedString.Key: Any]) -> (UITextField) -> Void
            = set(\.defaultTextAttributes)
        
        public static let delegate: (UITextFieldDelegate?) -> (UITextField) -> Void
            = set(\.delegate)
        
        public static let font: (UIFont?) -> (UITextField) -> Void
            = set(\.font)
        
        public static let keyboardAppearance: (UIKeyboardAppearance) -> (UITextField) -> Void
            = set(\.keyboardAppearance)
        
        public static let keyboardType: (UIKeyboardType) -> (UITextField) -> Void
            = set(\.keyboardType)
        
        public static let leftView: (UIView?) -> (UITextField) -> Void
            = set(\.leftView)
        
        public static let rightView: (UIView?) -> (UITextField) -> Void
            = set(\.rightView)
        
        public static let minimumFontSize: (CGFloat) -> (UITextField) -> Void
            = set(\.minimumFontSize)
        
        public static let pasteDelegate: (UITextPasteDelegate?) -> (UITextField) -> Void
            = set(\.pasteDelegate)
        
        public static let returnKeyType: (UIReturnKeyType) -> (UITextField) -> Void
            = set(\.returnKeyType)
        
        public static let selectionAffinity: (UITextStorageDirection) -> (UITextField) -> Void
            = set(\.selectionAffinity)
        
        public static let spellCheckingType: (UITextSpellCheckingType) -> (UITextField) -> Void
            = set(\.spellCheckingType)
        
        public static let textAlignment: (NSTextAlignment) -> (UITextField) -> Void
            = set(\.textAlignment)
        
        public static let textColor: (UIColor?) -> (UITextField) -> Void
            = set(\.textColor)
        
        public static let tintColor: (UIColor) -> (UITextField) -> Void
            = set(\.tintColor)
        
        public static let textContentType: (UITextContentType) -> (UITextField) -> Void
            = set(\.textContentType)
    }
    
    struct TextView {
        public static let adjustsFontForContentSizeCategory: (Bool) -> (UITextView) -> Void
            = set(\.adjustsFontForContentSizeCategory)
        
        public static let allowsEditingTextAttributes: (Bool) -> (UITextView) -> Void
            = set(\.allowsEditingTextAttributes)
        
        public static let clearsOnInsertion: (Bool) -> (UITextView) -> Void
            = set(\.clearsOnInsertion)
        
        public static let enablesReturnKeyAutomatically: (Bool) -> (UITextView) -> Void
            = set(\.enablesReturnKeyAutomatically)
        
        public static let isSecureTextEntry: (Bool) -> (UITextView) -> Void
            = set(\.isSecureTextEntry)
        
        public static let isEditable: (Bool) -> (UITextView) -> Void
            = set(\.isEditable)
        
        public static let isSelectable: (Bool) -> (UITextView) -> Void
            = set(\.isSelectable)
        
        public static let attributedText: (NSAttributedString) -> (UITextView) -> Void
            = set(\.attributedText)
        
        public static let autocapitalizationType: (UITextAutocapitalizationType) -> (UITextView) -> Void
            = set(\.autocapitalizationType)
        
        public static let autocorrectionType: (UITextAutocorrectionType) -> (UITextView) -> Void
            = set(\.autocorrectionType)
        
        public static let font: (UIFont?) -> (UITextView) -> Void
            = set(\.font)
        
        public static let text: (String) -> (UITextView) -> Void
            = set(\.text)
        
        public static let keyboardAppearance: (UIKeyboardAppearance) -> (UITextView) -> Void
            = set(\.keyboardAppearance)
        
        public static let keyboardType: (UIKeyboardType) -> (UITextView) -> Void
            = set(\.keyboardType)
        
         public static let pasteDelegate: (UITextPasteDelegate?) -> (UITextView) -> Void
            = set(\.pasteDelegate)
        
        public static let returnKeyType: (UIReturnKeyType) -> (UITextView) -> Void
            = set(\.returnKeyType)
        
        public static let selectionAffinity: (UITextStorageDirection) -> (UITextView) -> Void
            = set(\.selectionAffinity)
        
        public static let spellCheckingType: (UITextSpellCheckingType) -> (UITextView) -> Void
            = set(\.spellCheckingType)
        
        public static let textAlignment: (NSTextAlignment) -> (UITextView) -> Void
            = set(\.textAlignment)
        
        public static let textColor: (UIColor?) -> (UITextView) -> Void
            = set(\.textColor)
        
        public static let tintColor: (UIColor) -> (UITextView) -> Void
            = set(\.tintColor)
        
        public static let textContentType: (UITextContentType) -> (UITextView) -> Void
            = set(\.textContentType)
        
        public static let linkTextAttributes: ([NSAttributedString.Key: Any]) -> (UITextView) -> Void
            = set(\.linkTextAttributes)
        
        public static let dataDetectorTypes: (UIDataDetectorTypes) -> (UITextView) -> Void
            = set(\.dataDetectorTypes)
        
        public static let delegate: (UITextViewDelegate) -> (UITextView) -> Void
            = set(\.delegate)
        
        public static let maximumNumberOfLines: (Int) -> (UITextView) -> Void
            = set(\.textContainer.maximumNumberOfLines)
        
        public static let smartDashesType: (UITextSmartDashesType) -> (UITextView) -> Void
            = set(\.smartDashesType)
        
        public static let smartQuotesType: (UITextSmartQuotesType) -> (UITextView) -> Void
            = set(\.smartQuotesType)
    }
    
    struct Label {
        public static let adjustsFontForContentSizeCategory: (Bool) -> (UILabel) -> Void
            = set(\.adjustsFontForContentSizeCategory)
        
        public static let adjustsFontSizeToFitWidth: (Bool) -> (UILabel) -> Void
            = set(\.adjustsFontSizeToFitWidth)
        
        public static let allowsDefaultTighteningForTruncation: (Bool) -> (UILabel) -> Void
            = set(\.allowsDefaultTighteningForTruncation)
        
        public static let attributedText: (NSAttributedString?) -> (UILabel) -> Void
            = set(\.attributedText)
        
        public static let font: (UIFont) -> (UILabel) -> Void
            = set(\.font)
        
        public static let textAlignment: (NSTextAlignment) -> (UILabel) -> Void
            = set(\.textAlignment)
        
        public static let text: (String?) -> (UILabel) -> Void
            = set(\.text)
        
        public static let textColor: (UIColor) -> (UILabel) -> Void
            = set(\.textColor)
        
        public static let tintColor: (UIColor) -> (UILabel) -> Void
            = set(\.tintColor)
        
        public static let shadowColor: (UIColor?) -> (UILabel) -> Void
            = set(\.shadowColor)
        
        public static let highlightedTextColor: (UIColor?) -> (UILabel) -> Void
            = set(\.highlightedTextColor)
        
        public static let baselineAdjustment: (UIBaselineAdjustment) -> (UILabel) -> Void
            = set(\.baselineAdjustment)
        
        public static let lineBreakMode: (NSLineBreakMode) -> (UILabel) -> Void
            = set(\.lineBreakMode)
        
        public static let numberOfLines: (Int) -> (UILabel) -> Void
            = set(\.numberOfLines)
    }
}
