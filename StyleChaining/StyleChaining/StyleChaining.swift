//
//  StyleChaining.swift
//  Library
//
//  Created by Evgeniy Abashkin on 27/01/2019.
//

@discardableResult
public func with<A: AnyObject>(_ value: A, _ configure: (A) -> Void) -> A {
    configure(value)
    return value
}

precedencegroup ForwardApplication {
    associativity: left
    higherThan: DefaultPrecedence
}

infix operator |>

/**
 Apply function to object.
 
 - parameters:
 - o: Object.
 - g: Function to apply.
 
 - returns: Result object.
 */
public func |> <A: AnyObject>(
    _ o: A,
    g: @escaping (A) -> Void
    ) -> A {
    return with(o, g)
}

precedencegroup SingleTypeComposition {
    associativity: left
    higherThan: ForwardApplication
}

infix operator <>: SingleTypeComposition

/**
 Diamond operator. Combine functions.
 
 var a = Object()
 
 f(a)
 g(a)
 
 equals to:
 
 let z = f <> g
 z(a)
 
 - parameters:
 - f: Function to combine.
 - g: Function to combine.
 
 - returns: Combined function.
 */
public func <> <A: AnyObject>(
    f: @escaping (A) -> Void,
    g: @escaping (A) -> Void
    ) -> (A) -> Void {
    return { a in
        f(a)
        g(a)
    }
}

/**
 Wrap property setter in closure
 */
public func set<T: AnyObject, V>(
    _ keyPath: WritableKeyPath<T, V>
    ) -> (V) -> (T) -> Void {
    return { value in
        return { object in
            var object = object
            object[keyPath: keyPath] = value
        }
    }
}

/**
 Set property by key path
 */
public func set<T: AnyObject, V>(
    _ keyPath: WritableKeyPath<T, V>,
    _ value: V
    ) -> (T) -> Void {
    return { object in
        var object = object
        object[keyPath: keyPath] = value
    }
}
